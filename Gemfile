source 'https://rubygems.org'

gem 'rails', '4.2.0'

# authentication
gem 'cancancan'

# Use HAML for views
gem 'haml-rails', '~> 0.8'
# Use SimpleForm for Forms
gem 'simple_form'
gem 'cocoon' # nested_form helper

# Draper as model decorator
gem 'draper'

# Carrierwave for file uploads
gem 'carrierwave'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
gem 'bootstrap-sass'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
gem 'coffee-script-source', '1.8.0'

# Use jquery as the JavaScript library
gem 'jquery-rails'

# exports
gem 'axlsx_rails'
gem 'prawn'
gem 'prawn-table'
gem 'prawnto'
gem 'rqrcode_png'
gem 'prawn-qrcode'

# for windows time zones
gem 'tzinfo-data'

# hold versions for windows binaries
gem 'nokogiri', '1.6.6.2'
gem 'json', '1.8.2'
gem 'bcrypt', '3.1.10'
gem 'sqlite3', '1.3.10'

group :development do
  gem 'guard-rspec'
  gem 'rubocop', require: false
  gem 'faker'
end

group :development, :test do

  gem 'pry-debugger'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
end

group :test do
  # Test with Rspec and Capybara
  gem 'rspec-rails'
  gem 'rspec-collection_matchers'
  gem 'database_cleaner'
  gem 'connection_pool'
  gem 'capybara'
  gem 'poltergeist'
  gem 'factory_girl_rails', '~> 4.0'
end

